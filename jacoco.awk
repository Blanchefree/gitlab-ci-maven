{
  instructions += $4 + $5;
  covered += $5
}
END {
  print covered, "/", instructions, "instructions covered";
  print "code coverage:", 100*covered/instructions, "%"
}
