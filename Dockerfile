FROM registry.gitlab.com/byuhbll/apps/openj9-jdk11

ARG MAVEN_VERSION=3.6.3
ARG NODEJS_VERSION=10

ENV JAVA_HOME=/opt/java

# install maven
RUN cd /opt && \
  curl -LO http://www.apache.org/dist/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz && \
  tar xzf apache-maven-$MAVEN_VERSION-bin.tar.gz && \
  ln -s apache-maven-$MAVEN_VERSION maven && \
  ln -s /opt/maven/bin/mvn /usr/bin && \
  rm apache-maven-$MAVEN_VERSION-bin.tar.gz

COPY jacoco.awk /jacoco.awk
COPY settings.xml /root/.m2/settings.xml

# install node
RUN cd /opt && \
  curl -o setup -sL https://rpm.nodesource.com/setup_${NODEJS_VERSION}.x && \
  chmod a+x setup && \
  ./setup && \
  yum install nodejs git -y

ENTRYPOINT []

CMD ["/bin/bash"]
